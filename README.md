# sellsy-test-front

This template should help get you started developing with Vue 3 in Vite.

## Réalisation du test et remarque

Les maquettes mobile et desktop ont été intégrées. L'ouverture de la fenêtre modale des filtres pour la version mobile est fonctionnelle.

Filtre sur genre fonctionnel sur un seul genre à la fois. De même pour le filtre sur le sport.

Filtres sur prix, couleurs non fonctionnels.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
